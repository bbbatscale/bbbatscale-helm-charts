===========
 Changelog
===========

.. contents:: Versions
  :depth: 1

1.0.0 (2023-01-10)
==================

Other
-----
* move helm charts into dedicated repo (`4c9e041 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/4c9e04189097caf4d57cb5d60f76eba660c95337>`_)
* update app version to 4.10.6 (`3de8f7d <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/3de8f7d5643e9b2f31042dd2854b9c76c36ffd8d>`_)

`1.0.1 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/1.0.0...1.0.1>`_ (2023-02-21)
==================================================================================================

Other
-----
* update app version to 4.11.0 (`015768f <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/015768f6fe03988e88b18363e4e3153de3758e30>`_)

`2.0.0 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/1.0.1...2.0.0>`_ (2023-03-19)
==================================================================================================

⚠ BREAKING CHANGES
------------------
* **templates:** update cronjob api version from `batch/v1beta1`:code: to `batch/v1`:code:

Other
-----
* **templates:** update cronjob api version from `batch/v1beta1`:code: to `batch/v1`:code: (`d207641 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/d20764123f30b0a3b925b59d39fb163070a5af22>`_)

`3.0.0 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/2.0.0...3.0.0>`_ (2023-05-30)
==================================================================================================

⚠ BREAKING CHANGES
------------------
* **values:** The options `bbbatscale.media.{secretKey,subPath,pvc}`:code: for the local media storage backend have been moved to `bbbatscale.media.local.{secretKey,subPath,pvc}`:code:\.

Features
--------
* **values:** add the ability to specify additional volumes and environment variables (`d366cdd <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/d366cdd172ca621331375abb7373b45183194cc6>`_)
* **values:** add the ability to specify the credentials to use a S3 bucket as storage backend (`fe6d788 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/fe6d78855ae48d2b9adb3cd9159573d3e76b8fe2>`_)
* **values:** add the ability to specify the time zone for the backup database cronjob (`b55fcc1 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/b55fcc1fbfe01c1e7b1f8e4e430d257dbe290900>`_)

Other
-----
* **templates:** move email secrets into a kubernetes secret (`bb96d36 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/bb96d36724cd30afd6b237d494945230e539507b>`_)
* update app version to 4.12.0 (`86af5d7 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/86af5d7beb9eab3eee0971f326c9b11aff998386>`_)

`3.0.1 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.0...3.0.1>`_ (2023-06-30)
==================================================================================================

Other
-----
* update app version to 4.13.0 (`95ce157 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/95ce157df19203d2c8633571e23f62c8f0b26e71>`_)

`3.0.2 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.1...3.0.2>`_ (2023-07-07)
==================================================================================================

Other
-----
* update app version to 4.13.1 (`aaa1300 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/aaa1300c666e7fbecc4493e0dc7ddf6051a27596>`_)

`3.0.3 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.2...3.0.3>`_ (2023-09-21)
==================================================================================================

Other
-----
* update app version to 4.13.2 (`c929be6 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/c929be6ecede5f6d7fdd58d1313f28e49343301c>`_)

`3.0.4 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.3...3.0.4>`_ (2023-10-09)
==================================================================================================

Other
-----
* update app version to 4.13.3 (`bf69d24 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/bf69d24de4ad303976f22cefe6b03dbe2ca33add>`_)

`3.0.5 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.4...3.0.5>`_ (2023-10-13)
==================================================================================================

Other
-----
* update app version to 4.13.4 (`c493232 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/c4932322b12736627889629fec5a077d65b898a7>`_)

`3.0.6 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.5...3.0.6>`_ (2023-10-13)
==================================================================================================

Other
-----
* update app version to 4.13.5 (`0cf63cb <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/0cf63cb3de66f360be3d4cbb5824b14df311788b>`_)

`3.0.7 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.6...3.0.7>`_ (2023-11-20)
==================================================================================================

Other
-----
* add the ability to specify the ingress class name (`80abc62 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/80abc6236e6a2dea148b6876451ad23bc76a3866>`_)

`3.0.8 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.7...3.0.8>`_ (2023-11-22)
==================================================================================================

Other
-----
* adapt chart to revised logging capabilities of BBB@Scale (`0fa52ec <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/0fa52ecf61d998f82ae37ef5424914ee6b1f7dd0>`_)
* update app version to 4.14.0 (`ef11e23 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/ef11e239c721ae4743f9bf6841687957e541fbe3>`_)

`3.0.9 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.8...3.0.9>`_ (2024-04-11)
==================================================================================================

Other
-----
* update app version to 4.15.0 (`88c8a1d <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/88c8a1dc77530e708cc80ba18b6ea6141e777d0d>`_)

`3.0.10 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.9...3.0.10>`_ (2024-05-27)
====================================================================================================

Other
-----
* update app version to 4.15.1 (`18c2a2b <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/18c2a2b97ba4aa5595e11b1bf1f5324f7a07fe67>`_)

`3.0.11 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.10...3.0.11>`_ (2024-05-27)
=====================================================================================================

Bug Fixes
---------
* fix wrong BBB@Scale version (`ae175d9 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/ae175d9e04da70bddb3cdda94de391962450b20b>`_)

`3.0.12 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/compare/3.0.11...3.0.12>`_ (2025-01-31)
=====================================================================================================

Other
-----
* update app version to 4.16.0 (`4b04c86 <https://gitlab.com/bbbatscale/bbbatscale-helm-charts/commit/4b04c86ea2509af18a752d91189e3a56d3cc183c>`_)
