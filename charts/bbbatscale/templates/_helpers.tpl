{{/* BBB@Scale render template values */}}
{{- define "bbbatscale.render.template" -}}
{{- if kindIs "string" .value }}
{{- tpl .value .context }}
{{- else }}
{{- tpl (.value | toYaml) .context }}
{{- end }}
{{- end -}}

{{/* BBB@Scale returns the supplied secret or string as value for an env variable entry */}}
{{- define "bbbatscale.asEnvValueFromSecretOrString" -}}
{{ if kindIs "string" . -}}
value: {{ . | quote }}
{{- else -}}
valueFrom:
  secretKeyRef:
    name: {{ .secretName | quote }}
    key: {{ .secretKey | quote }}
{{- end }}
{{- end -}}

{{/* BBB@Scale database name as value for an env variable entry */}}
{{- define "bbbatscale.postgresql.database.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.postgresql.database }}
{{- end -}}

{{/* BBB@Scale database host as value for an env variable entry */}}
{{- define "bbbatscale.postgresql.host.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.postgresql.host }}
{{- end -}}

{{/* BBB@Scale database password as value for an env variable entry */}}
{{- define "bbbatscale.postgresql.password.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.postgresql.password }}
{{- end -}}

{{/* BBB@Scale database port as value for an env variable entry */}}
{{- define "bbbatscale.postgresql.port.asEnvValue" -}}
{{ with .Values.postgresql.port | default 5432 -}}
{{ if (kindIs "int" .) -}}
value: {{ . | quote }}
{{- else }}
{{- include "bbbatscale.asEnvValueFromSecretOrString" . }}
{{- end }}
{{- end }}
{{- end -}}

{{/* BBB@Scale database username as value for an env variable entry */}}
{{- define "bbbatscale.postgresql.username.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.postgresql.username }}
{{- end -}}

{{/* BBB@Scale database host as value for an env variable entry */}}
{{- define "bbbatscale.redis.host.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.redis.host }}
{{- end -}}

{{/* BBB@Scale database password as value for an env variable entry */}}
{{- define "bbbatscale.redis.password.asEnvValue" -}}
{{- include "bbbatscale.asEnvValueFromSecretOrString" .Values.redis.password }}
{{- end -}}

{{/* BBB@Scale database port as value for an env variable entry */}}
{{- define "bbbatscale.redis.port.asEnvValue" -}}
{{ with .Values.redis.port | default 6379 -}}
{{ if (kindIs "int" .) -}}
value: {{ . | quote }}
{{- else }}
{{- include "bbbatscale.asEnvValueFromSecretOrString" . }}
{{- end }}
{{- end }}
{{- end -}}

{{/* BBB@Scale environment variables */}}
{{- define "bbbatscale.env" -}}
- name: POSTGRES_DB
  {{- include "bbbatscale.postgresql.database.asEnvValue" . | nindent 2 }}
- name: POSTGRES_USER
  {{- include "bbbatscale.postgresql.username.asEnvValue" . | nindent 2 }}
- name: POSTGRES_PASSWORD
  {{- include "bbbatscale.postgresql.password.asEnvValue" . | nindent 2 }}
- name: POSTGRES_HOST
  {{- include "bbbatscale.postgresql.host.asEnvValue" . | nindent 2 }}
- name: POSTGRES_PORT
  {{- include "bbbatscale.postgresql.port.asEnvValue" . | nindent 2 }}
- name: REDIS_HOST
  {{- include "bbbatscale.redis.host.asEnvValue" . | nindent 2 }}
- name: REDIS_PORT
  {{- include "bbbatscale.redis.port.asEnvValue" . | nindent 2 }}
- name: REDIS_PASSWORD
  {{- include "bbbatscale.redis.password.asEnvValue" . | nindent 2 }}
- name: DJANGO_SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: djangoSecretKey
- name: RECORDINGS_SECRET
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: recordingsSecret
- name: DJANGO_SETTINGS_MODULE
  value: {{ .Values.bbbatscale.djangoSettingsModule }}
- name: SECURE_PROXY_SSL_HEADER
  value: HTTP_X_FORWARDED_PROTO
{{- if .Values.bbbatscale.webhooks.enabled }}
- name: WEBHOOKS
  value: enabled
{{- end }}
{{- if .Values.bbbatscale.media.enabled }}
- name: BBBATSCALE_MEDIA
  value: enabled
- name: BBBATSCALE_MEDIA_STORAGE_TYPE
  value: {{ .Values.bbbatscale.media.storageType | quote }}
- name: BBBATSCALE_MEDIA_URL_EXPIRY_SECONDS
  value: {{ .Values.bbbatscale.media.expirySeconds | quote }}
{{- if eq .Values.bbbatscale.media.storageType "local" }}
- name: BBBATSCALE_MEDIA_SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: bbbatscaleMediaSecretKey
{{- else }}
- name: BBBATSCALE_MEDIA_S3_ACCESS_ID
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: bbbatscaleMediaS3AccessId
- name: BBBATSCALE_MEDIA_S3_SECRET_KEY
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: bbbatscaleMediaS3SecretKey
- name: BBBATSCALE_MEDIA_S3_BUCKET_NAME
  value: {{ .Values.bbbatscale.media.s3.bucketName | quote }}
{{- with .Values.bbbatscale.media.s3.endpointUrl }}
- name: BBBATSCALE_MEDIA_S3_ENDPOINT_URL
  value: {{ . | quote }}
{{- end }}
{{- end }}
{{- end }}
- name: EMAIL_HOST
  value: {{ .Values.bbbatscale.email.host | quote }}
- name: EMAIL_PORT
  value: {{ .Values.bbbatscale.email.port | quote }}
{{- if .Values.bbbatscale.email.username }}
- name: EMAIL_HOST_USER
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: emailHostUser
{{- end }}
{{- if .Values.bbbatscale.email.password }}
- name: EMAIL_HOST_PASSWORD
  valueFrom:
    secretKeyRef:
      name: bbbatscale-secret
      key: emailHostPassword
{{- end }}
- name: EMAIL_USE_TLS
  value: {{ ternary "true" "false" .Values.bbbatscale.email.useTls | quote }}
- name: EMAIL_USE_SSL
  value: {{ ternary "true" "false" .Values.bbbatscale.email.useSsl | quote }}
{{- if .Values.bbbatscale.email.sslCertPem }}
- name: EMAIL_SSL_CERTFILE
  value: /bbbatscale/BBBatScale/settings/emailSslCert.pem
{{- end }}
{{- if .Values.bbbatscale.email.sslKeyPem }}
- name: EMAIL_SSL_KEYFILE
  value: /bbbatscale/BBBatScale/settings/emailSslKey.pem
{{- end }}
- name: EMAIL_SENDER_ADDRESS
  value: {{ .Values.bbbatscale.email.senderAddress | quote }}
- name: BBBATSCALE_HTTP_PROXY_COUNT
  value: {{ .Values.bbbatscale.httpProxyCount | quote }}
- name: BBBATSCALE_LOGGING_INCLUDE_REQUEST_QUERY
  value: {{ .Values.bbbatscale.logging.includeRequestQuery | quote }}
{{- if .Values.bbbatscale.logging.gelf.enabled }}
- name: BBBATSCALE_LOGGING_GELF_HOST
  value: {{ .Values.bbbatscale.logging.gelf.host | quote }}
- name: BBBATSCALE_LOGGING_GELF_PROTOCOL
  value: {{ .Values.bbbatscale.logging.gelf.protocol | quote }}
- name: BBBATSCALE_LOGGING_GELF_PORT
  value: {{ .Values.bbbatscale.logging.gelf.port | quote }}
{{- with .Values.bbbatscale.logging.gelf.facility }}
- name: BBBATSCALE_LOGGING_GELF_FACILITY
  value: {{ . | quote }}
{{- end }}
{{- if not (lower .Values.bbbatscale.logging.gelf.protocol | regexMatch "^(tcp|tls)$") }}
- name: BBBATSCALE_LOGGING_GELF_COMPRESS
  value: {{ .Values.bbbatscale.logging.gelf.compress | quote }}
{{- end }}
{{- if eq "tls" (lower .Values.bbbatscale.logging.gelf.protocol) }}
- name: BBBATSCALE_LOGGING_GELF_TLS_CLIENT_CERT_FILE
  value: /bbbatscale/BBBatScale/settings/gelf-tls/tls.crt
- name: BBBATSCALE_LOGGING_GELF_TLS_CLIENT_KEY_FILE
  value: /bbbatscale/BBBatScale/settings/gelf-tls/tls.key
{{- if .Values.bbbatscale.logging.gelf.tls.validateServer }}
- name: BBBATSCALE_LOGGING_GELF_TLS_VALIDATE_SERVER_CERT
  value: "true"
- name: BBBATSCALE_LOGGING_GELF_TLS_CA_CERTS_FILE
  value: /bbbatscale/BBBatScale/settings/gelf-tls/ca.crt
{{- end }}
{{- end }}
{{- if eq "http" (lower .Values.bbbatscale.logging.gelf.protocol) }}
- name: BBBATSCALE_LOGGING_GELF_HTTP_PATH
  value: {{ .Values.bbbatscale.logging.gelf.http.path | quote }}
- name: BBBATSCALE_LOGGING_GELF_HTTP_TIMEOUT
  value: {{ .Values.bbbatscale.logging.gelf.http.timeout | quote }}
{{- end }}
{{- end }}
{{- end -}}

{{/* BBB@Scale config volume */}}
{{- define "bbbatscale.configVolume" -}}
- name: bbbatscale-config
  configMap:
    name: bbbatscale-configmap
    defaultMode: 0444
{{- end -}}

{{/* BBB@Scale config volume mounts */}}
{{- define "bbbatscale.configVolumeMounts" -}}
{{ if .Values.bbbatscale.logging.config -}}
- name: bbbatscale-config
  readOnly: true
  mountPath: /bbbatscale/BBBatScale/settings/logging_config.py
  subPath: loggingConfig
{{- end }}
{{- end -}}

{{/* BBB@Scale secrets volume */}}
{{- define "bbbatscale.secretsVolume" -}}
- name: bbbatscale-secrets
  secret:
    secretName: bbbatscale-secret
    defaultMode: 0400
{{- end -}}

{{/* BBB@Scale secrets volume mounts */}}
{{- define "bbbatscale.secretsVolumeMounts" -}}
{{- if .Values.bbbatscale.email.sslCertPem }}
- name: bbbatscale-secrets
  readOnly: true
  mountPath: /bbbatscale/BBBatScale/settings/emailSslCert.pem
  subPath: emailSslCertPem
{{- end }}
{{- if .Values.bbbatscale.email.sslKeyPem }}
- name: bbbatscale-secrets
  readOnly: true
  mountPath: /bbbatscale/BBBatScale/settings/emailSslKey.pem
  subPath: emailSslKeyPem
{{- end }}
{{- end -}}

{{/* BBB@Scale gelf tls certificate volume */}}
{{- define "bbbatscale.gelfTLSCertificateVolume" -}}
{{- if and .Values.bbbatscale.logging.gelf.enabled (eq "tls" (lower .Values.bbbatscale.logging.gelf.protocol)) }}
- name: bbbatscale-gelf-tls-certificate
  secret:
    secretName: {{ .Values.bbbatscale.logging.gelf.tls.secretName | quote }}
    defaultMode: 0400
{{- end }}
{{- end -}}

{{/* BBB@Scale secrets volume mounts */}}
{{- define "bbbatscale.gelfTLSCertificateVolumeMounts" -}}
{{- if and .Values.bbbatscale.logging.gelf.enabled (eq "tls" (lower .Values.bbbatscale.logging.gelf.protocol)) }}
- name: bbbatscale-gelf-tls-certificate
  readOnly: true
  mountPath: /bbbatscale/BBBatScale/settings/gelf-tls/
{{- end }}
{{- end -}}

{{/* BBB@Scale media volume */}}
{{- define "bbbatscale.mediaVolume" -}}
{{ if and .Values.bbbatscale.media.enabled (eq .Values.bbbatscale.media.storageType "local") -}}
- name: bbbatscale-media
  persistentVolumeClaim:
    {{- if kindIs "string" .Values.bbbatscale.media.local.pvc }}
    claimName: {{ include "bbbatscale.render.template" (dict "value" .Values.bbbatscale.media.local.pvc "context" .) | quote }}
    {{- else }}
    claimName: bbbatscale-media-pvc
    {{- end }}
{{- end }}
{{- end -}}

{{/* BBB@Scale media volume mounts */}}
{{- define "bbbatscale.mediaVolumeMounts" -}}
{{ if and .Values.bbbatscale.media.enabled (eq .Values.bbbatscale.media.storageType "local") -}}
- name: bbbatscale-media
  readOnly: {{ ternary "true" "false" (default false .nginx) }}
  mountPath: {{ ternary "/var/www/bbbatscale/media/" "/bbbatscale/media/" (default false .nginx) }}
  {{- with .Values.bbbatscale.media.local.subPath }}
  subPath: {{ . | quote }}
  {{- end }}
{{- end }}
{{- end -}}

{{/* BBB@Scale database backup volume */}}
{{- define "bbbatscale.databaseBackupVolume" -}}
- name: bbbatscale-database-backup
  persistentVolumeClaim:
    {{- if kindIs "string" .pvc }}
    claimName: {{ include "bbbatscale.commonLabels" (dict "value" .pvc "context" $) | quote }}
    {{- else }}
    claimName: bbbatscale-database-backup-pvc
    {{- end }}
{{- end -}}

{{/* BBB@Scale database backup volume mounts */}}
{{- define "bbbatscale.databaseBackupVolumeMounts" -}}
- name: bbbatscale-database-backup
  mountPath: /bbbatscale/database-backup/
  {{- with .subPath }}
  subPath: {{ . | quote }}
  {{- end }}
{{- end -}}

{{/* BBB@Scale common volumes */}}
{{- define "bbbatscale.commonVolumes" -}}
{{ include "bbbatscale.configVolume" . }}
{{ include "bbbatscale.secretsVolume" . }}
{{ include "bbbatscale.gelfTLSCertificateVolume" . }}
{{- end -}}

{{/* BBB@Scale common volume mounts */}}
{{- define "bbbatscale.commonVolumeMounts" -}}
{{ include "bbbatscale.configVolumeMounts" . }}
{{ include "bbbatscale.secretsVolumeMounts" . }}
{{ include "bbbatscale.gelfTLSCertificateVolumeMounts" . }}
{{- end -}}

{{/* BBB@Scale common labels */}}
{{- define "bbbatscale.commonLabels" -}}
app.kubernetes.io/managed-by: {{ .Release.Service | quote }}
app.kubernetes.io/instance: {{ .Release.Name | quote }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
helm.sh/chart: {{ printf "%s-%s" .Chart.Name (.Chart.Version | replace "+" "_") | quote }}
helm.sh/revision: {{ .Release.Revision | quote }}
{{- end -}}
