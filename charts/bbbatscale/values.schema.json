{
  "$schema": "https://json-schema.org/draft-07/schema#",
  "title": "BBB@Scale Helm Values",
  "description": "Schema for the BBB@Scale Helm Chart values.yaml.",
  "definitions": {
    "imagePullPolicy": {
      "type": "string",
      "enum": [
        "Always",
        "Never",
        "IfNotPresent"
      ]
    },
    "deployment": {
      "type": "object",
      "properties": {
        "replicaCount": {
          "type": "integer"
        },
        "autoscaling": {
          "type": "object",
          "properties": {
            "enabled": {
              "type": "boolean"
            },
            "minReplicas": {
              "type": "integer",
              "minimum": 0
            },
            "maxReplicas": {
              "type": "integer",
              "minimum": 1
            },
            "targetCPUUtilizationPercentage": {
              "type": "integer",
              "minimum": 0,
              "maximum": 100
            }
          },
          "required": [
            "enabled"
          ]
        },
        "image": {
          "type": "string",
          "minLength": 1
        },
        "imageTag": {
          "type": "string"
        },
        "imagePullPolicy": {
          "$ref": "#/definitions/imagePullPolicy"
        },
        "imagePullSecrets": {
          "type": "array",
          "items": {
            "type": "string"
          },
          "minItems": 0
        },
        "annotations": {
          "type": "object"
        },
        "podAnnotations": {
          "type": "object"
        },
        "resources": {
          "type": "object"
        },
        "env": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/env"
          },
          "minItems": 0
        },
        "volumes": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/volume"
          },
          "minItems": 0
        },
        "volumeMounts": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/volumeMount"
          },
          "minItems": 0
        }
      }
    },
    "secretOrString": {
      "oneOf": [
        {
          "type": "string",
          "minLength": 1
        },
        {
          "type": "object",
          "properties": {
            "secretName": {
              "type": "string",
              "minLength": 1
            },
            "secretKey": {
              "type": "string",
              "minLength": 1
            }
          },
          "required": [
            "secretName",
            "secretKey"
          ]
        }
      ]
    },
    "manifests": {
      "type": "array",
      "items": {
        "oneOf": [
          {
            "type": "object"
          },
          {
            "type": "string"
          }
        ]
      }
    },
    "pvc": {
      "oneOf": [
        {
          "type": "string"
        },
        {
          "type": "object",
          "properties": {
            "storageClass": {
              "oneOf": [
                {
                  "type": "null"
                },
                {
                  "type": "string"
                }
              ]
            },
            "accessModes": {
              "type": "array",
              "minItems": 1,
              "items": {
                "type": "string",
                "oneOf": [
                  {
                    "const": "ReadWriteOnce"
                  },
                  {
                    "const": "ReadOnlyMany"
                  },
                  {
                    "const": "ReadWriteMany"
                  },
                  {
                    "const": "ReadWriteOncePod"
                  }
                ]
              }
            },
            "size": {
              "type": "string",
              "pattern": "^(\\d+\\.?|\\d*\\.\\d+)(Ki|Mi|Gi|Ti|Pi|Ei|[eE]\\+?(\\d+\\.?|\\d*\\.\\d+)|[kMGTPE])?$"
            },
            "annotations": {
              "type": "object"
            },
            "selector": {
              "type": "object"
            }
          },
          "required": [
            "accessModes",
            "size"
          ]
        }
      ]
    },
    "pvcRW": {
      "allOf": [
        {
          "$ref": "#/definitions/pvc"
        },
        {
          "type": "object",
          "properties": {
            "accessModes": {
              "type": "array",
              "items": {
                "type": "string",
                "not": {
                  "const": "ReadOnlyMany"
                }
              }
            }
          }
        }
      ]
    },
    "backup": {
      "type": "object",
      "properties": {
        "enabled": {
          "type": "boolean"
        }
      },
      "if": {
        "properties": {
          "enabled": {
            "const": true
          }
        }
      },
      "then": {
        "properties": {
          "enabled": {
            "const": true
          },
          "schedule": {
            "type": "string",
            "minLength": 1
          },
          "timeZone": {
            "oneOf": [
              {
                "type": "null"
              },
              {
                "type": "string",
                "minLength": 1
              }
            ]
          },
          "image": {
            "type": "string",
            "minLength": 1
          },
          "imagePullPolicy": {
            "anyOf": [
              {
                "$ref": "#/definitions/imagePullPolicy"
              },
              {
                "type": "string",
                "minLength": 1
              }
            ]
          },
          "imagePullSecrets": {
            "oneOf": [
              {
                "type": "array",
                "items": {
                  "type": "string"
                },
                "minItems": 0
              },
              {
                "type": "object",
                "properties": {
                  "extractValue": {
                    "type": "string",
                    "minLength": 1
                  }
                }
              }
            ]
          },
          "volume": {
            "type": "object",
            "properties": {
              "subPath": {
                "type": "string"
              },
              "pvc": {
                "$ref": "#/definitions/pvcRW"
              }
            },
            "required": [
              "pvc"
            ]
          }
        },
        "required": [
          "schedule",
          "image",
          "volume"
        ]
      },
      "required": [
        "enabled"
      ]
    },
    "env": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        }
      },
      "oneOf": [
        {
          "properties": {
            "value": {
              "type": "string"
            }
          },
          "required": [
            "value"
          ]
        },
        {
          "properties": {
            "valueFrom": {
              "type": "object"
            }
          },
          "required": [
            "valueFrom"
          ]
        }
      ],
      "required": [
        "name"
      ]
    },
    "volume": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        }
      },
      "required": [
        "name"
      ]
    },
    "volumeMount": {
      "type": "object",
      "properties": {
        "mountPath": {
          "type": "string"
        },
        "name": {
          "type": "string"
        },
        "mountPropagation": {
          "type": "string"
        },
        "readOnly": {
          "type": "boolean"
        }
      },
      "oneOf": [
        {
          "not": {
            "anyOf": [
              {
                "properties": {
                  "subPath": {}
                },
                "required": [
                  "subPath"
                ]
              },
              {
                "properties": {
                  "subPathExpr": {}
                },
                "required": [
                  "subPathExpr"
                ]
              }
            ]
          }
        },
        {
          "properties": {
            "subPath": {
              "type": "string"
            }
          },
          "required": [
            "subPath"
          ]
        },
        {
          "properties": {
            "subPathExpr": {
              "type": "string"
            }
          },
          "required": [
            "subPathExpr"
          ]
        }
      ],
      "required": [
        "mountPath",
        "name"
      ]
    }
  },
  "type": "object",
  "properties": {
    "bbbatscale": {
      "type": "object",
      "allOf": [
        {
          "$ref": "#/definitions/deployment"
        },
        {
          "properties": {
            "domains": {
              "type": "array",
              "items": {
                "type": "string"
              },
              "minItems": 1
            },
            "djangoSettingsModule": {
              "type": "string"
            },
            "djangoSecretKey": {
              "type": "string",
              "minLength": 1
            },
            "recordingsSecret": {
              "type": "string",
              "minLength": 1
            },
            "httpProxyCount": {
              "type": "integer"
            },
            "logging": {
              "type": "object",
              "properties": {
                "config": {
                  "type": "string"
                },
                "includeRequestQuery": {
                  "type": "boolean"
                },
                "gelf": {
                  "type": "object",
                  "properties": {
                    "enabled": {
                      "type": "boolean"
                    }
                  },
                  "if": {
                    "properties": {
                      "enabled": {
                        "const": true
                      }
                    }
                  },
                  "then": {
                    "properties": {
                      "host": {
                        "type": "string",
                        "minLength": 1
                      },
                      "protocol": {
                        "type": "string",
                        "enum": [
                          "UDP",
                          "TCP",
                          "TLS",
                          "HTTP"
                        ]
                      },
                      "port": {
                        "type": "integer",
                        "minimum": 1,
                        "maximum": 65535
                      },
                      "facility": {
                        "type": "string"
                      }
                    },
                    "allOf": [
                      {
                        "if": {
                          "not": {
                            "properties": {
                              "protocol": {
                                "enum": [
                                  "TCP",
                                  "TLS"
                                ]
                              }
                            }
                          }
                        },
                        "then": {
                          "properties": {
                            "compress": {
                              "type": "boolean"
                            }
                          },
                          "required": [
                            "compress"
                          ]
                        }
                      },
                      {
                        "if": {
                          "properties": {
                            "protocol": {
                              "const": "TLS"
                            }
                          }
                        },
                        "then": {
                          "properties": {
                            "tls": {
                              "type": "object",
                              "properties": {
                                "secretName": {
                                  "type": "string",
                                  "minLength": 1
                                },
                                "validateServer": {
                                  "type": "boolean"
                                }
                              },
                              "required": [
                                "secretName",
                                "validateServer"
                              ]
                            }
                          },
                          "required": [
                            "tls"
                          ]
                        }
                      },
                      {
                        "if": {
                          "properties": {
                            "protocol": {
                              "const": "HTTP"
                            }
                          }
                        },
                        "then": {
                          "properties": {
                            "http": {
                              "type": "object",
                              "properties": {
                                "path": {
                                  "type": "string"
                                },
                                "timeout": {
                                  "type": "integer"
                                }
                              }
                            }
                          },
                          "required": [
                            "http"
                          ]
                        }
                      }
                    ],
                    "required": [
                      "host",
                      "protocol",
                      "port"
                    ]
                  },
                  "required": [
                    "enabled"
                  ]
                }
              }
            },
            "email": {
              "type": "object",
              "properties": {
                "host": {
                  "type": "string",
                  "minLength": 1
                },
                "port": {
                  "type": "integer",
                  "minimum": 1,
                  "maximum": 65535
                },
                "useTls": {
                  "type": "boolean"
                },
                "useSsl": {
                  "type": "boolean"
                },
                "senderAddress": {
                  "type": "string",
                  "minLength": 3,
                  "format": "idn-email"
                }
              },
              "allOf": [
                {
                  "oneOf": [
                    {
                      "properties": {
                        "username": {
                          "type": "null"
                        },
                        "password": {
                          "type": "null"
                        }
                      }
                    },
                    {
                      "properties": {
                        "username": {
                          "type": "string",
                          "minLength": 1
                        },
                        "password": {
                          "type": "string",
                          "minLength": 1
                        }
                      }
                    }
                  ]
                },
                {
                  "if": {
                    "properties": {
                      "useTls": {
                        "const": true
                      }
                    }
                  },
                  "then": {
                    "properties": {
                      "useSsl": {
                        "const": false
                      }
                    }
                  }
                },
                {
                  "if": {
                    "properties": {
                      "useSsl": {
                        "const": true
                      }
                    }
                  },
                  "then": {
                    "properties": {
                      "useTls": {
                        "const": false
                      }
                    }
                  }
                },
                {
                  "if": {
                    "properties": {
                      "useTls": {
                        "const": false
                      },
                      "useSsl": {
                        "const": false
                      }
                    }
                  },
                  "then": {
                    "properties": {
                      "sslCertPem": {
                        "type": "null"
                      },
                      "sslKeyPem": {
                        "type": "null"
                      }
                    }
                  },
                  "else": {
                    "properties": {
                      "sslCertPem": {
                        "oneOf": [
                          {
                            "type": "null"
                          },
                          {
                            "type": "string",
                            "minLength": 1
                          }
                        ]
                      }
                    },
                    "if": {
                      "properties": {
                        "sslCertPem": {
                          "type": "string"
                        }
                      }
                    },
                    "then": {
                      "properties": {
                        "sslKeyPem": {
                          "oneOf": [
                            {
                              "type": "null"
                            },
                            {
                              "type": "string",
                              "minLength": 1
                            }
                          ]
                        }
                      }
                    },
                    "else": {
                      "properties": {
                        "sslKeyPem": {
                          "type": "null"
                        }
                      }
                    }
                  }
                }
              ]
            },
            "webhooks": {
              "type": "object",
              "properties": {
                "enabled": {
                  "type": "boolean"
                },
                "worker": {
                  "$ref": "#/definitions/deployment"
                }
              },
              "required": [
                "enabled"
              ]
            },
            "media": {
              "type": "object",
              "properties": {
                "enabled": {
                  "type": "boolean"
                }
              },
              "if": {
                "properties": {
                  "enabled": {
                    "const": true
                  }
                }
              },
              "then": {
                "properties": {
                  "expirySeconds": {
                    "type": "integer",
                    "minimum": 0
                  },
                  "storageType": {
                    "type": "string",
                    "enum": [
                      "local",
                      "s3"
                    ]
                  }
                },
                "oneOf": [
                  {
                    "properties": {
                      "storageType": {
                        "const": "local"
                      },
                      "local": {
                        "type": "object",
                        "properties": {
                          "secretKey": {
                            "type": "string",
                            "minLength": 1
                          },
                          "subPath": {
                            "type": "string"
                          },
                          "pvc": {
                            "$ref": "#/definitions/pvcRW"
                          }
                        },
                        "required": [
                          "pvc",
                          "secretKey"
                        ]
                      }
                    },
                    "required": [
                      "local"
                    ]
                  },
                  {
                    "properties": {
                      "storageType": {
                        "const": "s3"
                      },
                      "expirySeconds": {
                        "minimum": 1
                      },
                      "s3": {
                        "type": "object",
                        "properties": {
                          "accessId": {
                            "type": "string",
                            "minLength": 1
                          },
                          "secretKey": {
                            "type": "string",
                            "minLength": 1
                          },
                          "bucketName": {
                            "type": "string",
                            "minLength": 1
                          },
                          "endpointUrl": {
                            "type": "string"
                          }
                        },
                        "required": [
                          "accessId",
                          "secretKey",
                          "bucketName"
                        ]
                      }
                    },
                    "required": [
                      "s3"
                    ]
                  }
                ],
                "required": [
                  "expirySeconds",
                  "storageType"
                ]
              },
              "required": [
                "enabled"
              ]
            }
          }
        }
      ],
      "required": [
        "domains",
        "djangoSecretKey",
        "recordingsSecret"
      ]
    },
    "postgresql": {
      "description": "External Postgres configurations.",
      "type": "object",
      "properties": {
        "database": {
          "$ref": "#/definitions/secretOrString"
        },
        "username": {
          "$ref": "#/definitions/secretOrString"
        },
        "password": {
          "$ref": "#/definitions/secretOrString"
        },
        "host": {
          "$ref": "#/definitions/secretOrString"
        },
        "port": {
          "oneOf": [
            {
              "$ref": "#/definitions/secretOrString"
            },
            {
              "type": "integer",
              "minimum": 1,
              "maximum": 65535
            }
          ]
        },
        "backup": {
          "$ref": "#/definitions/backup"
        }
      },
      "required": [
        "database",
        "username",
        "password",
        "host"
      ]
    },
    "redis": {
      "description": "Redis configurations (for more information see https://github.com/bitnami/charts/blob/master/bitnami/redis/README.md).",
      "type": "object",
      "properties": {
        "password": {
          "$ref": "#/definitions/secretOrString"
        },
        "host": {
          "$ref": "#/definitions/secretOrString"
        },
        "port": {
          "oneOf": [
            {
              "$ref": "#/definitions/secretOrString"
            },
            {
              "type": "integer",
              "minimum": 1,
              "maximum": 65535
            }
          ]
        }
      },
      "required": [
        "password",
        "host"
      ]
    },
    "nginx": {
      "type": "object",
      "allOf": [
        {
          "$ref": "#/definitions/deployment"
        },
        {
          "properties": {
            "xForwardedProto": {
              "type": "string"
            },
            "port": {
              "type": "integer",
              "minimum": 1,
              "maximum": 65535
            },
            "serviceType": {
              "type": "string",
              "enum": [
                "ClusterIP",
                "NodePort",
                "LoadBalancer",
                "ExternalName"
              ]
            },
            "additionalServerConfig": {
              "type": "string"
            }
          }
        }
      ]
    },
    "ingress": {
      "type": "object",
      "properties": {
        "enabled": {
          "type": "boolean"
        },
        "annotations": {
          "type": "object"
        },
        "tls": {
          "type": "array",
          "items": {
            "type": "object",
            "properties": {
              "secretName": {
                "type": "string"
              },
              "hosts": {
                "type": "array",
                "items": {
                  "type": "string"
                },
                "minItems": 1
              }
            }
          }
        }
      },
      "required": [
        "enabled"
      ]
    },
    "additionalManifests": {
      "description": "Additional kubernetes manifests to apply to the cluster.",
      "$ref": "#/definitions/manifests"
    }
  },
  "required": [
    "bbbatscale",
    "postgresql",
    "redis",
    "nginx",
    "ingress"
  ]
}
